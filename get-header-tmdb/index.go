package main
import (
	"encoding/json"
	"fmt"
	"net/http"
)
func toJSON(r *http.Response, target interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(target)
}
func getQS(r *http.Request, key string) (string, error) {
	keys, ok := r.URL.Query()[key]
	if !ok || len(keys[0]) < 1 {
		return "", fmt.Errorf("getQS Error")
	}
	return keys[0], nil
}
func Handler(w http.ResponseWriter, r *http.Request) {
	id, err := getQS(r, "id")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}
	key, err := getQS(r, "key")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}
	mediaType, err := getQS(r, "type")
	if err != nil {
		mediaType = "movie"
	}
	url := "https://api.themoviedb.org/3/" + mediaType + "/" + id + "?api_key=" + key
	w.Header().Set("REQUESTED_URL", url)
	resp, err := http.Get(url)
	if err != nil {
		w.WriteHeader(resp.StatusCode)
		return
	}
	if resp.StatusCode > 200 {
		w.WriteHeader(resp.StatusCode)
		return
	}
	var i map[string]interface{}
	toJSON(resp, &i)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "\"%v\"", i["backdrop_path"])
}
// func main() {
// 	http.HandleFunc("/", Handler)
// 	http.ListenAndServe(":80", nil)