const fmi = require('find-my-iphone');
const util = require('util');
const url = require('url');
async function alert_iPhone(icloud) {
  const getDevices = util.promisify(icloud.getDevices.bind(icloud));
  const alertDevice = util.promisify(icloud.alertDevice.bind(icloud));
  const devices = await getDevices();
  for (let device of devices) {
    if (device.deviceModel.includes('iphone')) {
      await alertDevice(device.id);
    }
  }
}
module.exports = (req, res) => {
  const { query } = url.parse(req.url, true);
  const { id, password } = query;
  if (!id || !password) {
    return res.end('Unauthorized');
  }
  const icloud = fmi.findmyphone;
  icloud.apple_id = id;
  icloud.password = password;
  alert_iPhone(icloud)
    .then(() => {
      res.end('Ringing iPhone...');
    })
    .catch(err => {
      res.end(err);
    });
};