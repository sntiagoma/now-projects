const cheerio = require('cheerio');
const axios = require('axios');
const htmlparser2 = require('htmlparser2');
module.exports = async (req, res) => {
  const { data: html } = await axios.get('https://waze.com/livemap');
  const dom = htmlparser2.parseDOM(html);
  const $ = cheerio.load(dom);
  const gon = $('#gon').text();
  const json = JSON.parse(gon);
  res.json(json.googleApiKey);
};