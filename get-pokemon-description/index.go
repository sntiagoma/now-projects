package handler

// package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gocolly/colly"
)

func toJSON(r *http.Response, target interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(target)
}

func getQS(r *http.Request, key string) (string, error) {
	keys, ok := r.URL.Query()[key]
	if !ok || len(keys[0]) < 1 {
		return "", fmt.Errorf("getQS Error")
	}
	return keys[0], nil
}

func Handler(w http.ResponseWriter, r *http.Request) {
	// LANG
	lang, err := getQS(r, "lang")
	if err != nil {
		lang = "es"
	}
	id, err := getQS(r, "id")
	if err != nil {
		id = "1"
	}
	url := "https://www.pokemon.com/" + lang + "/pokedex/" + id
	w.Header().Set("Content-Type", "application/json")
	c := colly.NewCollector()
	v := true
	// Find and visit all links
	c.OnHTML("div.version-descriptions p", func(e *colly.HTMLElement) {
		// e.Request.Visit(e.Attr("href"))
		if v {
			v = false
			text := strings.Trim(e.Text, " \n")
			fmt.Fprintf(w, "\"%v\"", strings.ReplaceAll(text, "\n", "\\n"))
		}
	})

	c.OnRequest(func(r *colly.Request) {
		// fmt.Printf("Visiting: %v", r.URL)
		// fmt.Fprintf(w, "Visiting: %v", r.URL)
	})

	c.Visit(url)
}

// func main() {
// 	http.HandleFunc("/", Handler)
// 	http.ListenAndServe(":80", nil)
// }
